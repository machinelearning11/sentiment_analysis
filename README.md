#### AIM : 

**Your task is to classify whether a given review has a positive or negative tone using naive
  Bayes classifier.**
  
**DATASET** 

[http://ai.stanford.edu/~amaas/data/sentiment/](http://ai.stanford.edu/~amaas/data/sentiment/)

**USAGE**

Test with **jdk Version 1.8.0_131 Java 8**
 
1.In the project directory in terminal type
  __cd out/production/Sentiment_Analysis/__
    
2.Next:
  **java com.company.Main -dataFolder <Absolute Path for Data> -testFolder <Absolute Path for tests> -useScore True/False -useStopWords True/False -useBinaryNB False/True**
    
    **Example:** java com.company.Main -dataFolder /home/shuttle3468/aclImdb/ -testFolder /home/shuttle3468/IdeaProjects/Sentiment_Analysis/logs/ -useScore True
    
3.Results in detail can be seen in logs Folder in your respository Directory


### OUTPUT

0. Using the given Expected Ratings

    - 11108.000000 + 1392.000000 = 12500.000000
    
    - Accuracy for positive tests = 88.864000%
    
    - Log Created : negative_reviews.txt
    
    - 2267.000000 + 10233.000000 = 12500.000000
    
    - Accuracy for negative tests = 81.864000%
    
1. Using Naive Bayes and StopWords (java com.company.Main -dataFolder /home/shuttle3468/aclImdb/ -testFolder /home/shuttle3468/IdeaProjects/Sentiment_Analysis/logs/ -useScore False -useStopWords True -useBinaryNB False)

    - **Accuracy for Positive Set of test Cases 71.024000%**
    
    - **Accuracy for Negative Set of test Cases 69.040000%**
    
    - **Precision for Negative Set of test Cases 82.449604%**
    
    - **Precision for Positive Set of test Cases 86.227661%**
    
    - **Recall for Negative Set of test Cases 85.887739%**
    
    - **Recall for Positive Set of test Cases 82.855810%**
    
    - **F-measure for Negative Set of test Cases 84.133561**
    
    - **F-measure for Positive Set of test Cases 84.508115**
    
2. Using Extended Binary Naive Bayes and StopWords (java com.company.Main -dataFolder /home/shuttle3468/aclImdb/ -testFolder /home/shuttle3468/IdeaProjects/Sentiment_Analysis/logs/ -useScore False -useStopWords True -useBinaryNB True)

    - **Accuracy for Positive Set of test Cases 69.784000%**
    
    - **Accuracy for Negative Set of test Cases 68.928000%**
    
    - **Precision for Negative Set of test Cases 83.206181%**
    
    - **Precision for Positive Set of test Cases 85.553158%**
    
    - **Recall for Negative Set of test Cases 85.399941%**
    
    - **Recall for Positive Set of test Cases 83.377939%**
    
    - **F-measure for Negative Set of test Cases 84.288789**
    
    - **F-measure for Positive Set of test Cases 84.451544**
    
3. Using Extended Binary Naive Bayes, StopWords and Scores ( 2 layer)

    - **Accuracy for Positive Set of test Cases 88.216000%**
    
    - **Accuracy for Negative Set of test Cases 86.088000%**
    
    - **Precision for Negative Set of test Cases 86.088000%**
    
    - **Precision for Positive Set of test Cases 88.216000%**
    
    - **Recall for Negative Set of test Cases 87.959784%**
    
    - **Recall for Positive Set of test Cases 86.377879%**
    
    - **F-measure for Negative Set of test Cases 87.013827**
    
    - **F-measure for Positive Set of test Cases 87.287264**
    
4. Using Extended Naive Bayes, StopWords and Scores ( 2 layer)

    - **Accuracy for Positive Set of test Cases 88.656000%**
    
    - **Accuracy for Negative Set of test Cases 85.304000%**
    
    - **Precision for Negative Set of test Cases 85.304000%**
    
    - **Precision for Positive Set of test Cases 88.656000%**
    
    - **Recall for Negative Set of test Cases 88.262561%**
    
    - **Recall for Positive Set of test Cases 85.780633%**
    
    - **F-measure for Negative Set of test Cases 86.758065**
    
    - **F-measure for Positive Set of test Cases 87.194618**


5. Using Extended Binary Naive Bayes without stopword Removal (java com.company.Main -dataFolder /home/shuttle3468/aclImdb/ -testFolder /home/shuttle3468/IdeaProjects/Sentiment_Analysis/logs/ -useScore False -useStopWords False -useBinaryNB True)
    
    - **Accuracy for Positive Set of test Cases 60.904000%**
    
    - **Accuracy for Negative Set of test Cases 66.160000%**
    
    - **Precision for Negative Set of test Cases 87.144362%**
    
    - **Precision for Positive Set of test Cases 80.731707%**
    
    - **Recall for Negative Set of test Cases 81.986716%**
    
    - **Recall for Positive Set of test Cases 86.188158%**
    
    - **F-measure for Negative Set of test Cases 84.486898**
    
    - **F-measure for Positive Set of test Cases 83.370750**
    
6.  Using Naive Bayes without stopword Removal (java com.company.Main -dataFolder /home/shuttle3468/aclImdb/ -testFolder /home/shuttle3468/IdeaProjects/Sentiment_Analysis/logs/ -useScore False -useStopWords False -useBinaryNB False)
    
    - **Accuracy for Positive Set of test Cases 65.296000%**

    - **Accuracy for Negative Set of test Cases 65.616000%**
    
    - **Precision for Negative Set of test Cases 83.925100%**
    
    - **Precision for Positive Set of test Cases 84.283354%**
    
    - **Recall for Negative Set of test Cases 84.348005%**
    
    - **Recall for Positive Set of test Cases 83.859036%**
    
    - **F-measure for Negative Set of test Cases 84.136021**
    
    - **F-measure for Positive Set of test Cases 84.070660**






 